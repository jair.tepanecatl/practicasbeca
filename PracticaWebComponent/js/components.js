class MiSearch extends HTMLElement{
    constructor(){
        super();
    }
    async connectedCallback(){
        let searchButton = document.getElementById('searchid'),
            clearButton = document.getElementById('clearid');

        searchButton.addEventListener('click', this.doSearch);

        clearButton.addEventListener('click', this.doClear);
      }
     doSearch()
        {
            const tableReg = document.getElementById('contenido');
            const searchText = document.getElementById('searchText').value.toLowerCase();
            let total = 0;
 
            // Recorremos todas las filas con contenido de la tabla
            for (let i = 1; i < tableReg.rows.length; i++) {
               
                let found = false;
                const cellsOfRow = tableReg.rows[i].getElementsByTagName('td');
                // Recorremos todas las celdas
                for (let j = 0; j < cellsOfRow.length && !found; j++) {
                    const compareWith = cellsOfRow[j].innerHTML.toLowerCase();
                    // Buscamos el texto en el contenido de la celda
                    if (searchText.length == 0 || compareWith.indexOf(searchText) > -1) {
                        found = true;
                        total++;
                    }
                }
                if (found) {
                    tableReg.rows[i].style.display = '';
                } else {
                    // si no ha encontrado ninguna coincidencia, esconde la
                    // fila de la tabla
                    tableReg.rows[i].style.display = 'none';
                }
            }
        }
        doClear(){
            const tableReg = document.getElementById('contenido');
            const searchText = " ";
            let total = 0;

 
            // Recorremos todas las filas con contenido de la tabla
            for (let i = 1; i < tableReg.rows.length; i++) {
               
                let found = false;
                const cellsOfRow = tableReg.rows[i].getElementsByTagName('td');
                // Recorremos todas las celdas
                for (let j = 0; j < cellsOfRow.length && !found; j++) {
                    const compareWith = cellsOfRow[j].innerHTML.toLowerCase();
                    // Buscamos el texto en el contenido de la celda
                    if (searchText.length == 0 || compareWith.indexOf(searchText) > -1) {
                        found = true;
                        total++;
                    }
                }
                if (found) {
                    tableReg.rows[i].style.display = '';
                } else {
                    // si no ha encontrado ninguna coincidencia, esconde la
                    // fila de la tabla
                    tableReg.rows[i].style.display = 'none';
                }
            }
        }

}
customElements.define('mi-search', MiSearch);

  
  class MiTabla extends HTMLElement  {
    constructor(){
        super();
    }
    async connectedCallback(){
      this.fillTable();
    }
    fillTable(){
        fetch('http://dummy.restapiexample.com/api/v1/employees')
        //fetch('http://dummy.restapiexample.com/api/v1/employee/1')
        .then(function(response){
        return response.json();
        })
        .then(function(data){
        tabla(data);
        //console.log('data = ', data);
        })
        .catch(function(err){
        console.error(err);
        });
        function tabla(persona){
            console.log(persona);
            let res= document.querySelector('#contenido');
            res.innerHTML = '';
            for(let item of persona.data){
                console.log(item);
                res.innerHTML+=`
                <tr>
                    <td>${item.employee_name}</td>
                    <td>${item.employee_salary}</td>
                    <td>${item.employee_age}</td>
                </tr>
                `}
            }
    }
    
}
customElements.define('mi-tabla', MiTabla);